<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title' => 'Pride and Prejudice',
                'author' => 'Jane Austen  ',
                'user_id' => '1',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Jane Eyre',
                'author' => 'Charlotte Brontë',
                'user_id' => '1',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Romeo and Juliet ',
                'author' => 'William Shakespeare',
                'user_id' => '2',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Gone with the Wind',
                'author' => 'Margaret Mitchell  ',
                'user_id' => '2',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Wuthering Heights ',
                'author' => 'Emily Brontë',
                'user_id' => '2',
                'created_at' => date('Y-m-d G:i:s')
            ]
        ]);
    }
}
