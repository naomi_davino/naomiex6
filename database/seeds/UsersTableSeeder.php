<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
            [
            'name' => 'Naomi',
            'email'=> 'Naomi@gmail.com',
            'password' => '12345',
            'created_at' => date('y-m-d G:i:s'),
            ],
            [
            'name' => 'Naomi2',
            'email'=> 'Naomi2@gmail.com',
            'password' => '12345',
            'created_at' => date('y-m-d G:i:s'),
            ],
        ]
        );
    }
}
