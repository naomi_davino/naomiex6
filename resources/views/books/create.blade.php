@extends('layouts.app')
@section('content')

<h1>Create a new book </h1>
<form method = 'post' action = "{{action('BookController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">Title : </label>
    <input type = "text"class = "form-control" name = "title">
    <label for = "title">Author : </label>
    <input type = "text"class = "form-control" name = "author">
</div>

<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>
</form>
@endsection