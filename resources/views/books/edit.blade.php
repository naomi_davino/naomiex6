@extends('layouts.app')
@section('content')

<h1>Edit book </h1>

<form method = 'post' action = "{{action('BookController@update', $book->id)}}">
<!--קוד מובנה שמגן נגד פריצות תקיפת CSRF -->
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Book to Update</label>
    <input type = "text" class = "form-control" name = "title" value = "{{$book->title}}">
</div>

<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>
</form>

<!-- מחיקת משימה-->
<form method = 'post' action = "{{action('BookController@destroy', $book->id)}}">
<!--קוד מובנה שמגן נגד פריצות תקיפת CSRF -->
@csrf
@method('DELETE')
<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>
</form>
@endsection